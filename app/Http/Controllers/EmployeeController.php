<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use App\Service\EmployeeService;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function postImportCSV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()->getMessages()], 412);
        }
        
        $employeeService = new EmployeeService();
        $employeeService->importFileCSV(request()->file('file'));

        return response()->json([
            'Success' => 'sucesso'
        ], 201);
    }
}
