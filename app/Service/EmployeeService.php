<?php

namespace App\Service;

use App\Models\Employee;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailEmployee;
use Illuminate\Support\Facades\Log;
use App\Exceptions\CSVExceptions;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EmployeeImport;
use App\Jobs\ImportCSV;
use Auth;

class EmployeeService
{
    private $employee;

    public function __construct()
    {
        $this->employee = new Employee();
    }
    
    public function updateDataEmployee($data)
    {
        $verifyIfExistsEmployee = $this->employee::where('email', $data[1])->first();

        $employeeToUpdate = $this->employee->find($verifyIfExistsEmployee['id']);
        $employeeToUpdate->name = $data[0];
        $employeeToUpdate->email = $data[1];
        $employeeToUpdate->document = $data[2];
        $employeeToUpdate->city = $data[3];
        $employeeToUpdate->state = $data[4];
        $employeeToUpdate->start_date = $data[5];
        $employeeToUpdate->save();
        
        try {
            Mail::to($data[1])->send(new SendMailEmployee($verifyIfExistsEmployee));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function hasEmailCSV($email, $document)
    {
        $countEmail = $this->employee::where('email', $email)
            ->where('document', $document)
            ->count();
            
        if ($countEmail > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIfExistsSameEmailOrDocument($arrayEmployees)
    {
        for ($i = 0; $i < count($arrayEmployees[0]) - 1; $i ++) {
            for ($j = $i + 1; $j < count($arrayEmployees[0]); $j ++) {
                if ($arrayEmployees[0][$i][1] == $arrayEmployees[0][$j][1] && $arrayEmployees[0][$i][0] != $arrayEmployees[0][$j][0]) {
                    throw new CSVExceptions('Duplicate Email: '.$arrayEmployees[0][$i][1] ." - " .$arrayEmployees[0][$j][0]);
                }
                if ($arrayEmployees[0][$i][2] == $arrayEmployees[0][$j][2] && $arrayEmployees[0][$i][0] != $arrayEmployees[0][$j][0]) {
                    throw new CSVExceptions('Duplicate CPF: '.$arrayEmployees[0][$i][2] ." - " .$arrayEmployees[0][$j][0]);
                }
            }
        }
    }

    public function importFileCSV($file)
    {
        $userId = Auth::id();

        $helperService = new HelperService();
        
        $excelEmployees = Excel::toArray(new EmployeeImport($userId), $file);
        $this->checkIfExistsSameEmailOrDocument($excelEmployees);

        foreach ($excelEmployees[0] as $excelEmployee) {
            if (!$helperService->validateEmailAddress($excelEmployee[1])) {
                throw new CSVExceptions('Invalid Email Address');
            }
            if (!$helperService->validateDocumentCSV($excelEmployee[2])) {
                throw new CSVExceptions('Invalid Document: '.$excelEmployee[2]);
            }
            if (!$helperService->validateDateCSV($excelEmployee[5])) {
                throw new CSVExceptions('Invalid Date: '.$excelEmployee[5].' Employee: '. $excelEmployee[0]);
            }
        }
        
        $pathCsv = $file->storeAs(
            'employee',
            'employee.csv'
        );

        ImportCSV::dispatch($pathCsv, $userId);
    }
}
