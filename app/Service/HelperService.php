<?php

namespace App\Service;

class HelperService
{
    public function validateDocumentCSV($document)
    {
        $cpf = preg_replace('/[^0-9]/is', '', $document);
        
        if (strlen($cpf) != 11) {
            return false;
        }
    
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
    
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }

    public function validateDateCSV($date)
    {
        if (strlen($date) < 8) {
            return false;
        } else {
            if (strpos($date, "/") !== false) {
                $partes = explode("/", $date);
                    
                $dia = (int) $partes[0];
                    
                $mes = (int) $partes[1];

                $ano = (int) isset($partes[2]) ? $partes[2] : 0;
        
                if (strlen($ano) < 4) {
                    return false;
                } else {
                    if (checkdate($mes, $dia, $ano)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
    }

    public function validateEmailAddress($email)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) ? false : true;
    }
}
