<?php

namespace App\Imports;

use App\Models\Employee;
use App\Service\EmployeeService;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class EmployeeImport implements ToModel, WithCustomCsvSettings, WithStartRow, WithChunkReading, ShouldQueue
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $employeeService = new EmployeeService();
        
        if ($employeeService->hasEmailCSV($row[1], $row[2])) {
            $employeeService->updateDataEmployee($row);
            return null;
        }
        
        return new Employee([
                'user_id' => $this->userId,
                'name' => $row[0],
                'email' => $row[1],
                'document' => $row[2],
                'city' => $row[3],
                'state' => $row[4],
                'start_date' => $row[5],
            ]);
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";",
            'input_encoding'         => 'ISO-8859-1',
            'enclosure'              => '"',
            'line_ending'            => PHP_EOL,
            'use_bom'                => false,
            'include_separator_line' => false,
            'excel_compatibility'    => true,
        ];
    }

    public function startRow(): int
    {
        return 2;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
