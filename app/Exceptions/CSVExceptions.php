<?php

namespace App\Exceptions;

use Exception;

class CSVExceptions extends Exception
{
  
  public function __construct(String $message){
      parent::__construct($message);
  }
}
