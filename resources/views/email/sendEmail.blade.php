<html>
    <body>
        <p>Olá!</p>
        <p></p>
        <p>Seus dados foram atualizados:</p>
        <p>Nome: {{$employee->name}}</p>
        <p>Email: {{$employee->email}}</p>
        <p>CPF: {{$employee->document}}</p>
        <p>Cidade: {{$employee->city}}</p>
        <p>Estado: {{$employee->state}}</p>
        <p>Data: {{$employee->start_date}}</p>
        <p>Att, <br>
        Api</p>
    </body>
</html>