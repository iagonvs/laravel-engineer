

### Configuração

Após clonar o repositório e adentrar o diretório do projeto, execute:

```shell
cp .env.example .env
php artisan passport:install --force
php artisan key:generate
php artisan migrate
php vendor/bin/phpunit
```
