<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
   
    public function test_WhenCreateANewUser_ShouldReturnStatus200()
    {
        $user = User::factory()->create();

        $this->actingAs($user, 'api')
                         ->get('/')
                         ->assertStatus(200);
    }
}
