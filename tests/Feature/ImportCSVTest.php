<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Employee;
use App\Imports\EmployeeImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class ImportCSVTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function test_WhenUploadNewFile_ShouldReturnStatus201()
    {
        Storage::fake('employees');

        $header = 'name;email;document;city;state;start_date';
        $row1 = 'Iago Neves;iago-neves8@hotmail.com;07103734577;Salvador;BA;20/05/2021';

        $content = implode("\n", [$header, $row1]);

        $file = UploadedFile::fake()->createWithContent(
            'test.csv',
            $content
        );

        $response = $this->call('POST', '/api/employees', ['file' => $file]);
        $response->assertStatus(201);
    }


    public function test_WhenUpdateAemployee_And_ShouldBeTheSameStartDate()
    {
        Storage::fake('employees');

        $header = 'name;email;document;city;state;start_date';
        $row1 = 'Iago Neves;iago-neves8@hotmail.com;07103734577;Salvador;BA;30/05/2021';

        $content = implode("\n", [$header, $row1]);

        $file = UploadedFile::fake()->createWithContent(
            'test.csv',
            $content
        );

        $excelEmployeesToUpdate = Excel::toArray(new EmployeeImport($this->user->id), $file);
        $documentFileToUpdate = $excelEmployeesToUpdate[0][0][2];
        $dateFileToUpdate = date_create_from_format('d/m/Y', $excelEmployeesToUpdate[0][0][5])->format('d/m/Y');

        $this->call('POST', '/api/employees', ['file' => $file]);

        $findDateInDatabase = Employee::where('document', $documentFileToUpdate)->select('start_date')->get();
        date_create_from_format('d/m/Y', $findDateInDatabase[0]['start_date'])->format('d/m/Y');
         
        $this->assertEquals($dateFileToUpdate, $findDateInDatabase[0]['start_date']);
    }
}
